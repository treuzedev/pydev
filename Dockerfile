FROM python:3.9.7-slim-buster

ENV DEBIAN_FRONTEND noninteractive

COPY requirements.txt requirements.txt

RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
        procps \
        curl \
        jq \
        nginx \
        build-essential \
        python-dev \
        make && \
    pip install --no-cache-dir -r requirements.txt && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["pip freeze"]
